package com.github.vmironov.news.util

import io.michaelrocks.lightsaber.Injector
import io.michaelrocks.lightsaber.Lightsaber

object InjectorHolder {
  private var injector: Injector? = null

  fun getOrCreateInjector(component: Any): Injector {
    if (injector == null) {
      injector = Lightsaber.get().createInjector(component)
    }
    return injector!!
  }

  fun getInjector(): Injector {
    return injector!!
  }

  fun <T : Any> injectMembers(target: T): T {
    return target.apply { getInjector().injectMembers(this) }
  }
}
