package com.github.vmironov.news.ui.articles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.vmironov.news.R
import com.github.vmironov.news.bindings.FetchingObservableCommand
import com.github.vmironov.news.bindings.property
import com.github.vmironov.news.core.ArticleDetails
import com.github.vmironov.news.databinding.ArticleDetailsFragmentBinding
import com.github.vmironov.news.jetpack.fragment
import com.github.vmironov.news.jetpack.getTypedArguments
import com.github.vmironov.news.jetpack.ignoreErrors
import com.github.vmironov.news.jetpack.uninitialized
import com.github.vmironov.news.lifecycle.FragmentLifecycle
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindObservableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindToLifecycleEagerly
import com.github.vmironov.news.model.RootModel
import com.github.vmironov.news.model.changes
import com.github.vmironov.news.rx.observe
import com.github.vmironov.news.ui.base.BindingFragment
import com.github.vmironov.news.ui.fetching.addFetchingFragment
import io.mironov.smuggler.AutoParcelable
import javax.inject.Inject

class ArticleDetailsFragment : BindingFragment<ArticleDetailsFragmentBinding>(), ArticleDetailsViewModel {
  @Inject private val root: RootModel = uninitialized()
  @Inject private val formatter: ArticlesFormatter = uninitialized()

  private val model by bindToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
    root.acquireArticleDetailsModel(getTypedArguments<Arguments>().article)
  }

  override var loading by property(true)
  override var title by property<CharSequence>("")
  override var subtitle by property<CharSequence>("")
  override var content by property<CharSequence>("")
  override var image by property("")

  override val onRefreshClick by bindToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
    FetchingObservableCommand(model)
  }

  init {
    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.values.observe {
        loading = it.unwrap() == null
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.changes.observe {
        onArticleChanged(it)
      }
    }

    bindObservableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.fetch().ignoreErrors()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    addFetchingFragment(R.id.container, model)
  }

  override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): ArticleDetailsFragmentBinding {
    return ArticleDetailsFragmentBinding.inflate(inflater, container, false)
  }

  override fun onBindingCreated(binding: ArticleDetailsFragmentBinding, savedInstanceState: Bundle?) {
    binding.model = this
  }

  private fun onArticleChanged(article: ArticleDetails) {
    title = formatter.formatTitle(article)
    subtitle = formatter.formatSubtitle(article)
    image = formatter.formatImageUrl(article)
    content = formatter.formatContent(article)
  }

  companion object {
    fun newInstance(article: String): ArticleDetailsFragment {
      return fragment(Arguments(article))
    }
  }

  private class Arguments(val article: String) : AutoParcelable
}
