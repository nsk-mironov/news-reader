package com.github.vmironov.news.util

import android.content.Context
import android.net.ConnectivityManager
import com.github.vmironov.news.jetpack.cast
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityWatcher @Inject private constructor(
    private val context: Context
) {
  fun status(): ConnectivityStatus {
    return try {
      val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE).cast<ConnectivityManager>()
      val network = manager.activeNetworkInfo ?: return ConnectivityStatus.OFFLINE

      when {
        network.isConnected -> ConnectivityStatus.CONNECTED
        network.isConnectedOrConnecting -> ConnectivityStatus.CONNECTING
        else -> ConnectivityStatus.OFFLINE
      }
    } catch (exception: SecurityException) {
      ConnectivityStatus.CONNECTED
    }
  }
}

enum class ConnectivityStatus {
  UNKNOWN, CONNECTED, CONNECTING, OFFLINE
}

