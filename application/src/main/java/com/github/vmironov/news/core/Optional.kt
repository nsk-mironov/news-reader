package com.github.vmironov.news.core

import com.github.vmironov.news.jetpack.cast

sealed class Optional<out T> {
  class Some<out T>(val value: T) : Optional<T>() {
    override fun equals(other: Any?) = other is Some<*> && other.value == value
    override fun hashCode() = value?.hashCode() ?: 0
    override fun toString() = "Some[$value]"
  }

  object None : Optional<Nothing>() {
    override fun toString() = "None"
  }

  inline fun <R> map(mapper: (T) -> R): Optional<R> {
    return when (this) {
      is Some -> some(mapper(value))
      is None -> none()
    }
  }

  inline fun <R> flatMap(mapper: (T) -> Optional<R>): Optional<R> {
    return when (this) {
      is Some -> mapper(value)
      is None -> none()
    }
  }

  fun unwrap(): T? {
    return when (this) {
      is Some -> value
      is None -> null
    }
  }

  companion object {
    fun <T> some(value: T): Optional<T> {
      return Some(value)
    }

    fun <T> none(): Optional<T> {
      return None.cast()
    }

    fun <T> wrap(value: T?): Optional<T> {
      return if (value != null) some(value) else none()
    }
  }
}
