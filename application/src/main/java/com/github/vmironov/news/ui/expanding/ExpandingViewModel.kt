package com.github.vmironov.news.ui.expanding

import android.databinding.Bindable
import com.github.vmironov.news.bindings.ObservableCommand
import com.github.vmironov.news.bindings.ObservableModel

interface ExpandingViewModel : ObservableModel {
  @get:Bindable val offscreenOffset: Int

  @get:Bindable val showContainer: Boolean
  @get:Bindable val showMore: Boolean
  @get:Bindable val showProgress: Boolean

  @get:Bindable val onShowMoreClick: ObservableCommand<Unit>
}
