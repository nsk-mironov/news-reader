package com.github.vmironov.news.model

import com.github.vmironov.news.core.ArticleDetails
import com.github.vmironov.news.core.Optional
import com.github.vmironov.news.endpoints.ArticlesEndpoint
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.storage.ArticleStorage
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface ArticleDetailsModel : Model<ArticleDetails>

class ArticleDetailsModelImpl(
    private val articleId: String,
    private val endpoint: ArticlesEndpoint,
    private val storage: ArticleStorage
) : ArticleDetailsModel, CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private var cached = Optional.none<ArticleDetails>()

  override val fetching = BehaviorSubject.createDefault(false)
  override val errors = PublishSubject.create<Throwable>()

  override val values: Observable<Optional<ArticleDetails>>
    get() = Observable.defer { content.startWith(cached) }

  private val content by lazy {
    storage.article(articleId)
        .map { Optional.wrap(it) }
        .doOnNext { cached = it }
        .share()
  }

  private val fetch by lazy {
    endpoint.article(articleId)
        .flatMap { storage.replace(it) }
        .doOnSubscribe { fetching.onNext(true) }
        .doOnError { errors.onNext(it) }
        .doAfterTerminate { fetching.onNext(false) }
        .doOnDispose { fetching.onNext(false) }
        .share()
  }

  override fun fetch(): Observable<Unit> {
    return fetch
  }
}
