package com.github.vmironov.news.model

import io.reactivex.Observable

interface CollectionModel<T : Any> : Model<List<T>> {
  val expandable: Observable<Boolean>
  val expanding: Observable<Boolean>

  fun expand(): Observable<Unit>
}
