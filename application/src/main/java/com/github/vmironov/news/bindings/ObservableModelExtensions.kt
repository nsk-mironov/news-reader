package com.github.vmironov.news.bindings

import android.databinding.Observable
import io.reactivex.disposables.Disposables
import kotlin.properties.ReadWriteProperty

fun <T> ObservableModel.property(
    initial: T,
    vararg bindings: String = EMPTY_ARRAY_STRING,
    unique: Boolean = true,
    self: Boolean = true,
    all: Boolean = false
): ReadWriteProperty<Any, T> {
  return ObservableModelProperty(initial, DataBindingMapping.convertToBindings(bindings), unique, self, all, this)
}

fun ObservableModel.changes(): io.reactivex.Observable<Unit> {
  return changes(0)
}

fun ObservableModel.changes(binding: String): io.reactivex.Observable<Unit> {
  return changes(DataBindingMapping.convertToBinding(binding))
}

fun ObservableModel.changes(binding: Int): io.reactivex.Observable<Unit> {
  return io.reactivex.Observable.create { emitter ->
    val callback = object : Observable.OnPropertyChangedCallback() {
      override fun onPropertyChanged(observable: Observable, field: Int) {
        if (field <= 0 || field == binding) {
          emitter.onNext(Unit)
        }
      }
    }

    addOnPropertyChangedCallback(callback)

    emitter.setDisposable(Disposables.fromAction {
      removeOnPropertyChangedCallback(callback)
    })
  }
}
