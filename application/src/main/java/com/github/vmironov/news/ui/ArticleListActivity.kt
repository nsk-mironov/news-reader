package com.github.vmironov.news.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.github.vmironov.news.R
import com.github.vmironov.news.databinding.ArticleListActivityBinding
import com.github.vmironov.news.jetpack.findOrAddFragment
import com.github.vmironov.news.ui.articles.ArticleListFragment
import com.github.vmironov.news.ui.base.BaseActivity

class ArticleListActivity : BaseActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    DataBindingUtil.setContentView<ArticleListActivityBinding>(this, R.layout.article_list_activity)

    findOrAddFragment(R.id.container) {
      ArticleListFragment.newInstance()
    }
  }
}
