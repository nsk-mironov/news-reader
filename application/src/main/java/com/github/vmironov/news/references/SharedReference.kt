package com.github.vmironov.news.references

import java.io.Closeable
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Proxy
import java.util.LinkedHashSet

@Suppress("AddVarianceModifier")
class SharedReference<T : Closeable> private constructor(
    private val clazz: Class<T>,
    private val factory: () -> T
) {
  private val lock = Object()

  private var value: T? = null
  private var counter: Int = 0

  init {
    require(clazz.isInterface) {
      "CountingReference can be only created for interfaces, but ${clazz.name} was provided"
    }
  }

  fun acquire(): T {
    return synchronized(lock) {
      if (value == null) {
        value = create(factory())
        counter = 0
      }
      counter += 1
      value!!
    }
  }

  private fun create(target: T): T {
    val loader = clazz.classLoader
    val interfaces = collectInterfaces(target)

    return clazz.cast(Proxy.newProxyInstance(loader, interfaces.toTypedArray()) { proxy, method, args ->
      if (method.name != Closeable::close.name || !args.orEmpty().isEmpty()) {
        return@newProxyInstance try {
          method.invoke(target, *args.orEmpty())
        } catch (exception: InvocationTargetException) {
          throw exception.targetException
        }
      }

      return@newProxyInstance synchronized(lock) {
        if (counter <= 0) {
          throw IllegalStateException("CountingReference is already closed!")
        }

        if (--counter == 0) {
          try {
            method.invoke(target, *args.orEmpty())
          } catch (exception: InvocationTargetException) {
            throw exception.targetException
          } finally {
            value = null
          }
        }

        null
      }
    })
  }

  private fun collectInterfaces(target: T): Set<Class<*>> {
    return LinkedHashSet<Class<*>>().apply {
      traverseHierarchy(target.javaClass) {
        if (!it.isInterface) {
          addAll(it.interfaces)
        }

        if (it.isInterface) {
          add(it)
        }
      }
    }
  }

  private inline fun traverseHierarchy(clazz: Class<*>, action: (Class<*>) -> Unit) {
    var current = clazz

    while (current != Any::class.java) {
      action(current)
      current = current.superclass
    }
  }

  companion object {
    fun <T : Closeable> createUnsafe(clazz: Class<T>, factory: () -> T): SharedReference<T> {
      return SharedReference(clazz, factory)
    }
  }
}
