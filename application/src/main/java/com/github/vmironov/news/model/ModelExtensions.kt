package com.github.vmironov.news.model

import com.github.vmironov.news.core.Optional
import com.github.vmironov.news.jetpack.filterIsInstance
import io.reactivex.Observable

val <T : Any> Model<T>.changes: Observable<T>
  get() = values.filterIsInstance<Optional.Some<T>>().map { it.value }
