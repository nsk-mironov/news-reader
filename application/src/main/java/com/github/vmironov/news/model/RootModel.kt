package com.github.vmironov.news.model

import com.github.vmironov.news.lifecycle.CloseableLifecycle
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.lifecycle.bindToLifecycleEagerly
import com.github.vmironov.news.references.SharedReference
import java.io.Closeable
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

interface RootModel {
  fun acquireArticleListModel(): ArticleListModel
  fun acquireArticleDetailsModel(id: String): ArticleDetailsModel
}

@Singleton
class RootModelImpl @Inject private constructor(
    private val articlesProvider: Provider<ArticleListModelImpl>,
    private val articlesCache: ModelCache<String, ArticleDetailsModel>
) : RootModel, CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private val articlesList = Models.share(ArticleListModel::class.java, articlesProvider)

  init {
    manage(articlesList)
  }

  override fun acquireArticleListModel(): ArticleListModel {
    return articlesList.acquire()
  }

  override fun acquireArticleDetailsModel(id: String): ArticleDetailsModel {
    return articlesCache.acquire(id)
  }

  private fun <T : Closeable> manage(reference: SharedReference<T>) {
    bindToLifecycleEagerly(CloseableLifecycle.Interval.CREATED) { reference.acquire() }
  }
}
