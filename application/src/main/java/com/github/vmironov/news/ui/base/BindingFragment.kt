package com.github.vmironov.news.ui.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.vmironov.news.bindings.ObservableModel
import com.github.vmironov.news.bindings.ObservableModelMixin

abstract class BindingFragment<B : ViewDataBinding> : BaseFragment(), ObservableModel by ObservableModelMixin() {
  protected val binding: B
    get() = DataBindingUtil.getBinding<B>(view)

  final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return onCreateBinding(inflater, container, savedInstanceState).root
  }

  final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    onBindingCreated(binding, savedInstanceState)
    onExecutePendingBindings(binding)
  }

  final override fun onDestroyView() {
    super.onDestroyView()
    onExecutePendingBindings(binding)
    onDestroyBinding(binding)
    onUnbindBinding(binding)
  }

  protected abstract fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): B

  protected open fun onBindingCreated(binding: B, savedInstanceState: Bundle?) {
    // nothing to do
  }

  protected open fun onDestroyBinding(binding: B) {
    // nothing to do
  }

  private fun onExecutePendingBindings(binding: B) {
    binding.executePendingBindings()
  }

  private fun onUnbindBinding(binding: B) {
    binding.unbind()
  }
}
