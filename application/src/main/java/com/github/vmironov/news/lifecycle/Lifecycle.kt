package com.github.vmironov.news.lifecycle

interface Lifecycle<L : Lifecycle<L>> : Comparable<L> {
  val valid: Boolean

  val expanding: Boolean
  val collapsing: Boolean
    get() = !expanding

  fun next(): L
  fun previous(): L

  fun interval(): LifecycleInterval<L>

  fun symmetric(): L {
    val interval = interval()
    return if (interval.start == this) interval.endInclusive else interval.start
  }

  fun before(event: L): Boolean {
    return this < event
  }

  fun after(event: L): Boolean {
    return this > event
  }

  fun nextOnPathTo(event: L): L {
    require(event.valid) { "$event is inaccessible from $this" }
    require(this != event) { "Source and destination events are equal" }

    if (!valid) {
      return next()
    }

    if (this == event.symmetric()) {
      return event
    }

    val fromInterval = interval()
    if (event in fromInterval) {
      return if (this == fromInterval.start) next() else fromInterval.start
    }

    return if (this == fromInterval.start) fromInterval.endInclusive else fromInterval.endInclusive.next()
  }
}
