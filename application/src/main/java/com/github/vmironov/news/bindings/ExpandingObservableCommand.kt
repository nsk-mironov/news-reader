package com.github.vmironov.news.bindings

import com.github.vmironov.news.jetpack.ignoreErrors
import com.github.vmironov.news.lifecycle.CloseableLifecycle
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindObservableToLifecycleEagerly
import com.github.vmironov.news.model.CollectionModel
import com.github.vmironov.news.rx.observe
import io.reactivex.subjects.PublishSubject

class ExpandingObservableCommand(private val model: CollectionModel<out Any>) : BaseObservableCommand<Unit>(), CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private val action = PublishSubject.create<Unit>()

  init {
    bindDisposableToLifecycleEagerly(CloseableLifecycle.Interval.CREATED) {
      model.expanding.observe {
        executing = it
      }
    }

    bindObservableToLifecycleEagerly(CloseableLifecycle.Interval.CREATED) {
      action.switchMap {
        model.expand().ignoreErrors()
      }
    }
  }

  override fun execute(argument: Unit) {
    action.onNext(argument)
  }
}
