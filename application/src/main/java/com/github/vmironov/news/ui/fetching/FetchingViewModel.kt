package com.github.vmironov.news.ui.fetching

import android.databinding.Bindable
import com.github.vmironov.news.bindings.ObservableCommand
import com.github.vmironov.news.bindings.ObservableModel

interface FetchingViewModel : ObservableModel {
  @get:Bindable val errorTitle: CharSequence
  @get:Bindable val errorMessage: CharSequence

  @get:Bindable val showContainer: Boolean
  @get:Bindable val showRefreshing: Boolean
  @get:Bindable val showError: Boolean

  @get:Bindable val onRefreshClick: ObservableCommand<Unit>
}
