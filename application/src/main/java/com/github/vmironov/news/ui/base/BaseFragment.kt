package com.github.vmironov.news.ui.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.github.vmironov.news.lifecycle.DelegateLifecycleAware
import com.github.vmironov.news.lifecycle.FragmentLifecycle
import com.github.vmironov.news.lifecycle.LifecycleAwareDelegate
import com.github.vmironov.news.util.InjectorHolder

open class BaseFragment : Fragment(), DelegateLifecycleAware<FragmentLifecycle> {
  override val lifecycle = LifecycleAwareDelegate(FragmentLifecycle.UNKNOWN)

  override fun onAttach(context: Context) {
    InjectorHolder.injectMembers(this)
    super.onAttach(context)
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.ATTACH)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.CREATE)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.VIEW_CREATE)
  }

  override fun onStart() {
    super.onStart()
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.START)
  }

  override fun onResume() {
    super.onResume()
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.RESUME)
  }

  override fun onPause() {
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.PAUSE)
    super.onPause()
  }

  override fun onStop() {
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.STOP)
    super.onStop()
  }

  override fun onDestroyView() {
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.VIEW_DESTROY)
    super.onDestroyView()
  }

  override fun onDestroy() {
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.DESTROY)
    super.onDestroy()
  }

  override fun onDetach() {
    lifecycle.notifyLifecycleEvent(FragmentLifecycle.DETACH)
    super.onDetach()
  }
}
