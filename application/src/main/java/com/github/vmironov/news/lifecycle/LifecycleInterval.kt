package com.github.vmironov.news.lifecycle

interface LifecycleInterval<L : Lifecycle<L>> : ClosedRange<L> {
  override operator fun contains(value: L): Boolean {
    return value in start..endInclusive
  }

  fun containsExclusive(value: L): Boolean {
    return value >= start && value < endInclusive
  }

  fun toRangeString(): String {
    return "[$start..$endInclusive})"
  }
}
