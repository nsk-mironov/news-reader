package com.github.vmironov.news.core

import io.mironov.smuggler.AutoParcelable

data class PagedCollection<T>(
    val items: List<T> = emptyList(),
    val token: String? = null
)

data class Article(
    val id: String = "",
    val title: String = "",
    val subtitle: String = "",
    val image: String = ""
) : AutoParcelable

data class ArticleDetails(
    val id: String = "",
    val title: String = "",
    val subtitle: String = "",
    val image: String = "",
    val content: String = ""
) : AutoParcelable
