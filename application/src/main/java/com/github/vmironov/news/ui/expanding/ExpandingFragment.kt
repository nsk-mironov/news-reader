package com.github.vmironov.news.ui.expanding

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.vmironov.news.bindings.ExpandingObservableCommand
import com.github.vmironov.news.bindings.property
import com.github.vmironov.news.databinding.ExpandingFragmentBinding
import com.github.vmironov.news.jetpack.find
import com.github.vmironov.news.jetpack.fragment
import com.github.vmironov.news.jetpack.getTypedArguments
import com.github.vmironov.news.jetpack.initializableOnce
import com.github.vmironov.news.lifecycle.FragmentLifecycle
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindToLifecycle
import com.github.vmironov.news.model.CollectionModel
import com.github.vmironov.news.rx.observe
import com.github.vmironov.news.ui.base.BindingFragment
import com.github.vmironov.news.ui.bindings.scrollChanges
import io.mironov.smuggler.AutoParcelable

class ExpandingFragment : BindingFragment<ExpandingFragmentBinding>(), ExpandingViewModel {
  private var expanding by property(false, all = true)
  private var expandable by property(false, all = true)

  override var offscreenOffset by property(0)

  override val showContainer: Boolean
    get() = showMore || showProgress

  override val showProgress: Boolean
    get() = expandable && expanding

  override val showMore: Boolean
    get() = expandable && !expanding

  override val onShowMoreClick by bindToLifecycle(FragmentLifecycle.Interval.CREATED) {
    ExpandingObservableCommand(model)
  }

  var model by initializableOnce<CollectionModel<*>> { model ->
    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.expanding.observe {
        expanding = it
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.expandable.observe {
        expandable = it
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.VIEW_CREATED) {
      val id = getTypedArguments<Arguments>().recycler
      val recycler = parentFragment.view!!.find<RecyclerView>(id)

      recycler.scrollChanges().observe {
        onScrolled(recycler)
      }
    }
  }

  override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): ExpandingFragmentBinding {
    return ExpandingFragmentBinding.inflate(inflater, container, false)
  }

  override fun onBindingCreated(binding: ExpandingFragmentBinding, savedInstanceState: Bundle?) {
    binding.model = this
  }

  private fun onScrolled(recycler: RecyclerView) {
    val range = recycler.computeVerticalScrollRange()
    val offset = recycler.computeVerticalScrollOffset()
    val extent = recycler.computeVerticalScrollExtent()

    val total = range.toFloat()
    val scrolled = offset.toFloat() + extent.toFloat()
    val remaining = total - scrolled
    val threshold = 0.8f

    if (remaining < extent * 5 || scrolled / total >= threshold) {
      if (expandable && !expanding) {
        onShowMoreClick.execute(Unit)
      }
    }

    offscreenOffset = Math.max(0, range - offset - extent)
  }

  companion object {
    fun newInstance(recycler: Int): ExpandingFragment {
      return fragment(Arguments(recycler))
    }
  }

  private data class Arguments(val recycler: Int) : AutoParcelable
}
