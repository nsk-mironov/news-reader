package com.github.vmironov.news.ui

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import com.github.vmironov.news.R
import com.github.vmironov.news.databinding.ArticleDetailsFragmentBinding
import com.github.vmironov.news.jetpack.findOrAddFragment
import com.github.vmironov.news.jetpack.getTypedArguments
import com.github.vmironov.news.jetpack.intent
import com.github.vmironov.news.ui.articles.ArticleDetailsFragment
import com.github.vmironov.news.ui.base.BaseActivity
import io.mironov.smuggler.AutoParcelable

class ArticleDetailsActivity : BaseActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    DataBindingUtil.setContentView<ArticleDetailsFragmentBinding>(this, R.layout.article_details_activity)

    findOrAddFragment(R.id.container) {
      ArticleDetailsFragment.newInstance(getTypedArguments<Arguments>().article)
    }
  }

  companion object {
    fun newIntent(context: Context, article: String): Intent {
      return intent<ArticleDetailsActivity>(context, Arguments(article))
    }
  }

  private class Arguments(val article: String) : AutoParcelable
}
