package com.github.vmironov.news.bindings

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class ObservableModelProperty<T>(
    private val initial: T,
    private val bindings: IntArray,
    private val unique: Boolean,
    private val self: Boolean,
    private val all: Boolean,
    private val model: ObservableModel
) : ReadWriteProperty<Any, T> {
  private var value = initial

  override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
    if (!unique || this.value != value) {
      this.value = value

      if (self && !all) {
        model.notifyPropertyChanged(DataBindingMapping.convertToBinding(property.name))
      }

      if (all) {
        model.notifyChange()
      }

      bindings.forEach {
        model.notifyPropertyChanged(it)
      }
    }
  }

  override fun getValue(thisRef: Any, property: KProperty<*>): T {
    return value
  }
}
