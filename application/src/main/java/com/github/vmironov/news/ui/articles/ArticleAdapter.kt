package com.github.vmironov.news.ui.articles

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.vmironov.news.bindings.property
import com.github.vmironov.news.core.Article
import com.github.vmironov.news.databinding.ArticleListItemBinding
import com.github.vmironov.news.jetpack.replaceAll
import com.github.vmironov.news.jetpack.uninitialized
import com.github.vmironov.news.lifecycle.CloseableLifecycle
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.model.ArticleListModel
import com.github.vmironov.news.rx.observe
import com.github.vmironov.news.util.LifecycleAwareAdapter
import com.github.vmironov.news.util.LifecycleAwareViewHolder
import com.github.vmironov.news.util.StableIds
import io.michaelrocks.lightsaber.Injector
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ArticleAdapterFactory @Inject private constructor(private val injector: Injector) {
  fun create(context: Context, collection: ArticleListModel): ArticleAdapter {
    return ArticleAdapter(context, collection).apply {
      injector.injectMembers(this)
    }
  }
}

class ArticleAdapter(
    private val context: Context,
    private val collection: ArticleListModel
) : LifecycleAwareAdapter<ArticleAdapter.ViewHolder>() {
  @Inject private val formatter: ArticlesFormatter = uninitialized()

  private val clicks = PublishSubject.create<Article>()
  private val articles = ArrayList<Article>()
  private val ids = StableIds(Article::id)

  init {
    setHasStableIds(true)
  }

  init {
    bindDisposableToLifecycleEagerly(CloseableLifecycle.Interval.CREATED) {
      collection.values.observe {
        articles.replaceAll(it.unwrap().orEmpty())
        notifyDataSetChanged()
      }
    }
  }

  fun clicks(): Observable<Article> {
    return clicks
  }

  override fun getItemCount(): Int {
    return articles.size
  }

  override fun getItemId(position: Int): Long {
    return ids.from(articles[position])
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val binding = ArticleListItemBinding.inflate(LayoutInflater.from(context), parent, false)
    val holder = ViewHolder(binding)

    binding.root.setOnClickListener {
      if (holder.adapterPosition != RecyclerView.NO_POSITION) {
        clicks.onNext(articles[holder.adapterPosition])
      }
    }

    return holder
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val article = articles[position]

    holder.title = formatter.formatTitle(article)
    holder.subtitle = formatter.formatSubtitle(article)
    holder.image = formatter.formatImageUrl(article)

    holder.binding.executePendingBindings()
  }

  class ViewHolder(val binding: ArticleListItemBinding) : LifecycleAwareViewHolder(binding.root), ArticleListItemViewModel {
    override var title by property<CharSequence>("")
    override var subtitle by property<CharSequence>("")
    override var image by property("")

    init {
      binding.model = this
    }
  }
}
