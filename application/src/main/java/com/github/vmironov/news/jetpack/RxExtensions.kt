package com.github.vmironov.news.jetpack

import io.reactivex.Observable

fun <T : Any> Observable<T>.ignoreValues(): Observable<T> {
  return ignoreElements().toObservable()
}

fun <T : Any> Observable<T>.ignoreErrors(): Observable<T> {
  return onErrorResumeNext(Observable.empty())
}

fun <T : Any> Observable<T>.ignoreCompletion(): Observable<T> {
  return concatWith(Observable.never())
}

fun <T : Any> Observable<T>.asUnitObservable(): Observable<Unit> {
  return map { Unit }
}

inline fun <reified T : Any> Observable<*>.filterIsInstance(): Observable<T> {
  return filter { it is T }.map { it as T }
}
