package com.github.vmironov.news.model

import com.github.vmironov.news.core.Optional
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import io.reactivex.Observable

interface Model<T : Any> : CloseableLifecycleAware {
  val values: Observable<Optional<T>>

  val fetching: Observable<Boolean>
  val errors: Observable<Throwable>

  fun fetch(): Observable<Unit>
}
