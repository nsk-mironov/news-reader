package com.github.vmironov.news

import android.app.Application
import android.content.Context
import android.content.res.Resources
import io.michaelrocks.lightsaber.Module
import io.michaelrocks.lightsaber.Provides

@Module
class ApplicationModule(private val application: Application) {
  @Provides fun provideApplication(): Application {
    return application
  }

  @Provides fun provideContext(): Context {
    return application
  }

  @Provides fun provideResources(): Resources {
    return application.resources
  }
}
