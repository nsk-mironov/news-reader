package com.github.vmironov.news.model

import com.github.vmironov.news.core.Article
import com.github.vmironov.news.core.Optional
import com.github.vmironov.news.core.PagedCollection
import com.github.vmironov.news.endpoints.ArticlesEndpoint
import com.github.vmironov.news.jetpack.asUnitObservable
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.storage.ArticleStorage
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

interface ArticleListModel : CollectionModel<Article>

class ArticleListModelImpl @Inject private constructor(
    private val endpoint: ArticlesEndpoint,
    private val storage: ArticleStorage
) : ArticleListModel, CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private var cached = Optional.none<PagedCollection<Article>>()

  override val fetching = BehaviorSubject.createDefault(false)
  override val errors = PublishSubject.create<Throwable>()

  override val expandable = BehaviorSubject.createDefault(false)
  override val expanding = BehaviorSubject.createDefault(false)

  override val values: Observable<Optional<List<Article>>>
    get() = Observable.defer { content.startWith(cached) }.map { it.map { it.items } }

  private val content by lazy {
    storage.articles()
        .map { Optional.wrap(PagedCollection(items = it, token = cached.unwrap()?.token)) }
        .doOnNext { cached = it }
        .share()
  }

  private val fetch by lazy {
    endpoint.articles(null, 50)
        .flatMap { save(it) }
        .doOnSubscribe { fetching.onNext(true) }
        .doOnError { errors.onNext(it) }
        .doAfterTerminate { fetching.onNext(false) }
        .doOnDispose { fetching.onNext(false) }
        .doOnNext { cached = Optional.wrap(it) }
        .doOnNext { expandable.onNext(it.token != null) }
        .asUnitObservable()
        .share()
  }

  private val expand by lazy {
    endpoint.articles(cached.unwrap()?.token, 50)
        .flatMap { save(it.copy(items = cached.unwrap()?.items.orEmpty() + it.items)) }
        .doOnSubscribe { expanding.onNext(true) }
        .doOnError { errors.onNext(it) }
        .doAfterTerminate { expanding.onNext(false) }
        .doOnDispose { expanding.onNext(false) }
        .doOnNext { cached = Optional.wrap(it) }
        .doOnNext { expandable.onNext(it.token != null) }
        .asUnitObservable()
        .share()
  }

  override fun fetch(): Observable<Unit> {
    return fetch
  }

  override fun expand(): Observable<Unit> {
    return expand
  }

  private fun save(value: PagedCollection<Article>): Observable<PagedCollection<Article>> {
    return storage.replace(value.items)
        .doOnNext { cached = Optional.some(value) }
        .map { value }
  }
}
