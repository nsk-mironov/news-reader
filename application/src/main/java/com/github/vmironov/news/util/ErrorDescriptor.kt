package com.github.vmironov.news.util

import android.content.Context
import com.github.vmironov.news.R
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorDescriptor @Inject private constructor(
    private val context: Context
) {
  fun getHumanReadableErrorTitle(throwable: Throwable): CharSequence {
    return context.getString(R.string.error_unknown_title)
  }

  fun getHumanReadableErrorMessage(throwable: Throwable): CharSequence {
    return context.getString(R.string.error_unknown_message)
  }

  fun getHumanReadableErrorToast(throwable: Throwable): CharSequence {
    return context.getString(R.string.error_unknown_toast)
  }
}
