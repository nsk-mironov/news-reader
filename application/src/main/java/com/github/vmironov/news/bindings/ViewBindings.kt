package com.github.vmironov.news.bindings

import android.databinding.BindingAdapter
import android.view.View

@BindingAdapter(*arrayOf("onClick", "clickable"), requireAll = false)
fun setOnClick(view: View, command: ObservableCommand<Unit>?, clickable: Boolean?) {
  if (command != null) {
    view.setOnClickListener { command.execute(Unit) }
  } else {
    view.setOnClickListener(null)
  }
  view.isClickable = clickable ?: true
}

@BindingAdapter(value = *arrayOf("visible", "inLayout"), requireAll = false)
fun setVisible(view: View, visible: Boolean?, inLayout: Boolean?) {
  view.visibility = when {
    visible ?: true -> View.VISIBLE
    inLayout ?: false -> View.INVISIBLE
    else -> View.GONE
  }
}
