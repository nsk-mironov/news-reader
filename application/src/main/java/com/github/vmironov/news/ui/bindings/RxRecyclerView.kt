package com.github.vmironov.news.ui.bindings

import android.support.v7.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.disposables.Disposables

fun RecyclerView.scrollChanges(): Observable<Unit> {
  return Observable.create { emitter ->
    val listener = object : RecyclerView.OnScrollListener() {
      override fun onScrolled(recycler: RecyclerView, dx: Int, dy: Int) {
        emitter.onNext(Unit)
      }
    }

    addOnScrollListener(listener)

    emitter.setDisposable(Disposables.fromAction {
      removeOnScrollListener(listener)
    })
  }
}
