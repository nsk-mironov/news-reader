package com.github.vmironov.news.jetpack

import android.view.View

inline fun <reified V : View> View.find(id: Int): V {
  return findViewById(id) as V
}

inline fun <reified V : View> View.findOptional(id: Int): V? {
  return findViewById(id) as? V
}
