package com.github.vmironov.news.rx

import android.os.Handler
import android.os.Looper
import android.os.Message
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.plugins.RxJavaPlugins
import java.util.concurrent.TimeUnit

private val handler = Handler(Looper.getMainLooper())
private val scheduler = HandlerScheduler(handler)

fun mainThreadScheduler(): Scheduler {
  return scheduler
}

class HandlerScheduler(private val handler: Handler) : Scheduler() {
  override fun createWorker(): Worker {
    return HandlerWorker(handler)
  }

  private class HandlerWorker(private val handler: Handler) : Worker() {
    private @Volatile var disposed = false

    override fun schedule(runnable: Runnable, delay: Long, unit: TimeUnit): Disposable {
      if (disposed) {
        return Disposables.disposed()
      }

      val scheduled = ScheduledRunnable(handler, RxJavaPlugins.onSchedule(runnable))
      val message = Message.obtain(handler, scheduled).apply {
        obj = this@HandlerWorker
      }

      if (delay == 0L && handler.looper == Looper.myLooper()) {
        scheduled.run()
        return Disposables.disposed()
      }

      handler.sendMessageDelayed(message, unit.toMillis(delay))

      if (isDisposed) {
        handler.removeCallbacks(scheduled)
        return Disposables.disposed()
      }

      return scheduled
    }

    override fun isDisposed(): Boolean {
      return disposed
    }

    override fun dispose() {
      handler.removeCallbacksAndMessages(this)
      disposed = true
    }
  }

  private class ScheduledRunnable(
      private val handler: Handler,
      private val delegate: Runnable
  ) : Runnable, Disposable {
    private @Volatile var disposed = false

    override fun run() {
      try {
        delegate.run()
      } catch (t: Throwable) {
        RxJavaPlugins.onError(IllegalStateException("Fatal Exception thrown on Scheduler.", t))
      }
    }

    override fun isDisposed(): Boolean {
      return disposed
    }

    override fun dispose() {
      handler.removeCallbacks(this)
      disposed = true
    }
  }
}
