package com.github.vmironov.news.ui.articles

import android.databinding.Bindable
import com.github.vmironov.news.bindings.ObservableCommand
import com.github.vmironov.news.bindings.ObservableModel

interface ArticleDetailsViewModel : ObservableModel {
  @get:Bindable val loading: Boolean

  @get:Bindable val title: CharSequence
  @get:Bindable val subtitle: CharSequence
  @get:Bindable val content: CharSequence
  @get:Bindable val image: String

  @get:Bindable val onRefreshClick: ObservableCommand<Unit>
}
