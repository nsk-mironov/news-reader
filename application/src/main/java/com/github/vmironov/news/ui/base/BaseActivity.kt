package com.github.vmironov.news.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.github.vmironov.news.lifecycle.ActivityLifecycle
import com.github.vmironov.news.lifecycle.DelegateLifecycleAware
import com.github.vmironov.news.lifecycle.LifecycleAwareDelegate
import com.github.vmironov.news.util.InjectorHolder

open class BaseActivity : AppCompatActivity(), DelegateLifecycleAware<ActivityLifecycle> {
  override val lifecycle = LifecycleAwareDelegate(ActivityLifecycle.UNKNOWN)

  override fun onCreate(savedInstanceState: Bundle?) {
    InjectorHolder.injectMembers(this)
    super.onCreate(savedInstanceState)
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.CREATE)
  }

  override fun onStart() {
    super.onStart()
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.START)
  }

  override fun onResume() {
    super.onResume()
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.RESUME)
  }

  override fun onPause() {
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.PAUSE)
    super.onPause()
  }

  override fun onStop() {
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.STOP)
    super.onStop()
  }

  override fun onDestroy() {
    lifecycle.notifyLifecycleEvent(ActivityLifecycle.DESTROY)
    super.onDestroy()
  }
}
