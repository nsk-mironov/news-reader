package com.github.vmironov.news.model

import com.github.vmironov.news.endpoints.ArticlesEndpoint
import com.github.vmironov.news.storage.ArticleStorage
import com.github.vmironov.news.storage.ArticleStorageImpl
import io.michaelrocks.lightsaber.Module
import io.michaelrocks.lightsaber.Provides

@Module
class ModelModule {
  @Provides fun provideRootModel(impl: RootModelImpl): RootModel {
    return impl
  }

  @Provides fun providerArticleStorage(impl: ArticleStorageImpl): ArticleStorage {
    return impl
  }

  @Provides fun provideArticleModelCache(endpoint: ArticlesEndpoint, storage: ArticleStorage): ModelCache<String, ArticleDetailsModel> {
    return ModelCache.lru(ArticleDetailsModel::class.java) {
      ArticleDetailsModelImpl(it, endpoint, storage)
    }
  }
}
