package com.github.vmironov.news.lifecycle

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class LifecycleAwareProperty<T, L : Lifecycle<L>>(
    val lifecycle: LifecycleAware<L>,
    val interval: LifecycleInterval<L>,
    val eager: Boolean
) : ReadOnlyProperty<Any, T>, (Lifecycle<L>) -> Unit {
  private var value: Any? = null

  init {
    lifecycle.onLifecycleStateChanged += this
  }

  override fun getValue(thisRef: Any, property: KProperty<*>): T {
    if (!interval.containsExclusive(lifecycle.lifecycleState)) {
      if (lifecycle.lifecycleState != interval.endInclusive || value == null) {
        throw IllegalStateException("""
          Unable to access property "${property.name}" in ${lifecycle.lifecycleState} state.
          Allowed states ${interval.toRangeString()}"""
        )
      }
    }

    createValue()
    @Suppress("UNCHECKED_CAST")
    return unescape(value) as T
  }

  override fun invoke(lifecycle: Lifecycle<L>) {
    if (lifecycle == interval.start && eager) {
      createValue()
    }

    if (lifecycle == interval.endInclusive && value != null) {
      @Suppress("UNCHECKED_CAST")
      onRelease(value as T)
      value = null
    }
  }

  protected abstract fun onAcquire(): T
  protected abstract fun onRelease(value: T)

  private fun createValue() {
    if (value == null) {
      value = escape(onAcquire())
    }
  }

  private fun escape(value: Any?): Any {
    return value ?: NULL_VALUE
  }

  private fun unescape(value: Any?): Any? {
    return if (value === NULL_VALUE) null else value
  }

  private object NULL_VALUE
}

abstract class BaseLifecycleAwareProperty<T, L : Lifecycle<L>>(
    lifecycle: LifecycleAware<L>,
    interval: LifecycleInterval<L>,
    eager: Boolean
) : LifecycleAwareProperty<T, L>(lifecycle, interval, eager) {
  override fun onRelease(value: T) = Unit
}

