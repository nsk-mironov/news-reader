package com.github.vmironov.news.core

import java.util.ArrayList

fun <T> event(initializer: () -> T): Event<T> {
  return StrongEvent({ it(initializer()) })
}

interface Event<T> {
  fun addObserver(observer: (T) -> Unit)
  fun removeObserver(observer: (T) -> Unit)
  fun tryAddObserver(observer: (T) -> Unit): Boolean
  fun tryRemoveObserver(observer: (T) -> Unit): Boolean
  fun hasObservers(): Boolean
  fun clearObservers()

  operator fun plusAssign(observer: (T) -> Unit) {
    addObserver(observer)
  }

  operator fun minusAssign(observer: (T) -> Unit) {
    removeObserver(observer)
  }

  operator fun invoke(data: T, reverse: Boolean = false)
}

abstract class BaseEvent<T>(
    protected val initializer: ((observer: (T) -> Unit) -> Unit)? = null
) : Event<T> {
  override fun addObserver(observer: (T) -> Unit) {
    val added = tryAddObserver(observer)
    check(added) { "Observer has already been added" }
  }

  override fun removeObserver(observer: (T) -> Unit) {
    val removed = tryRemoveObserver(observer)
    check(removed) { "Observer hasn't been added" }
  }
}

class StrongEvent<T>(
    initializer: (observer: (T) -> Unit) -> Unit = {}
) : BaseEvent<T>(initializer) {

  private var invoking = false
  private var pendingAddedObserversOffset = 0
  private var pendingRemovedObserversOffset = 0

  private val observers = RemoveRangeArrayList<(T) -> Unit>()

  override fun tryAddObserver(observer: (T) -> Unit): Boolean {
    if (invoking) {
      if (!addPendingObserver(observer)) {
        return false
      }
    } else {
      if (findObserver(0, observers.size, observer) != -1) {
        return false
      } else {
        observers.add(observer)
      }
    }
    initializer?.let { initial -> initial(observer) }
    return true
  }

  override fun tryRemoveObserver(observer: (T) -> Unit): Boolean {
    if (invoking) {
      return removePendingObserver(observer)
    } else {
      return removeObserverInRange(0, observers.size, observer)
    }
  }

  override fun invoke(data: T, reverse: Boolean) {
    check(!invoking) { "Nested invocation is not supported" }

    try {
      invoking = true
      pendingAddedObserversOffset = observers.size
      pendingRemovedObserversOffset = observers.size
      onInvocation(data, reverse)
      processPendingObservers()
    } finally {
      invoking = false
    }
  }

  override fun hasObservers(): Boolean =
      if (invoking) pendingAddedObserversOffset > 0 else observers.isNotEmpty()

  override fun clearObservers() {
    if (invoking) {
      observers.removeRange(pendingAddedObserversOffset, observers.size)
      pendingAddedObserversOffset = observers.size
      pendingRemovedObserversOffset = observers.size
      observers.addAll(observers)
    } else {
      observers.clear()
    }
  }

  private fun onInvocation(data: T, reverse: Boolean) {
    if (observers.isEmpty()) {
      return
    }

    if (reverse) {
      var i = observers.size - 1
      while (i >= 0) {
        observers[i](data)
        i -= 1
      }
    } else {
      for (i in 0..observers.size - 1) {
        observers[i](data)
      }
    }
  }

  private fun addPendingObserver(observer: (T) -> Unit): Boolean {
    fun insertPendingObserver() {
      observers.add(pendingRemovedObserversOffset, observer)
      ++pendingRemovedObserversOffset
    }

    if (findObserver(pendingAddedObserversOffset, pendingRemovedObserversOffset, observer) != -1) {
      return false
    } else if (findObserver(pendingRemovedObserversOffset, observers.size, observer) != -1) {
      insertPendingObserver()
      return true
    } else if (findObserver(0, pendingAddedObserversOffset, observer) != -1) {
      return false
    } else {
      insertPendingObserver()
      return true
    }
  }

  private fun removePendingObserver(observer: (T) -> Unit): Boolean {
    if (findObserver(0, pendingAddedObserversOffset, observer) != -1) {
      if (removeObserverInRange(pendingAddedObserversOffset, pendingRemovedObserversOffset, observer)) {
        --pendingRemovedObserversOffset
        return true
      } else if (findObserver(pendingRemovedObserversOffset, observers.size, observer) != -1) {
        return false
      } else {
        observers += observer
        return true
      }
    } else {
      if (removeObserverInRange(pendingAddedObserversOffset, pendingRemovedObserversOffset, observer)) {
        --pendingRemovedObserversOffset
        return true
      } else {
        return false
      }
    }
  }

  private fun removeObserverInRange(from: Int, to: Int, observer: (T) -> Unit): Boolean {
    val index = findObserver(from, to, observer)
    if (index == -1) {
      return false
    } else {
      observers.removeAt(index)
      return true
    }
  }

  private inline fun findObserver(from: Int, to: Int, observer: (T) -> Unit): Int {
    for (i in from..to - 1) {
      if (observers[i] === observer) {
        return i
      }
    }
    return -1
  }

  private fun processPendingObservers() {
    val pendingRemovedObserverCount = observers.size - pendingRemovedObserversOffset
    if (pendingRemovedObserverCount == 0) {
      return
    }

    for (i in 0..pendingRemovedObserverCount - 1) {
      val observer = observers.removeAt(observers.size - 1)
      val removed = removeObserverInRange(0, pendingAddedObserversOffset - i, observer)
      check(removed) { "Pending removed observer must be present in currently added observers" }
    }
  }

  private class RemoveRangeArrayList<T> : ArrayList<T>() {
    public override fun removeRange(fromIndex: Int, toIndex: Int) {
      super.removeRange(fromIndex, toIndex)
    }
  }
}
