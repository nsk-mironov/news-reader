package com.github.vmironov.news.endpoints

import com.github.vmironov.news.core.Article
import com.github.vmironov.news.core.ArticleDetails
import com.github.vmironov.news.core.PagedCollection
import com.github.vmironov.news.jetpack.given
import com.github.vmironov.news.rx.mainThreadScheduler
import com.github.vmironov.news.util.ConnectivityStatus
import com.github.vmironov.news.util.ConnectivityWatcher
import io.reactivex.Observable
import java.security.SecureRandom
import java.util.UUID
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

interface ArticlesEndpoint {
  fun articles(token: String?, count: Int): Observable<PagedCollection<Article>>

  fun article(id: String): Observable<ArticleDetails>
}

@Singleton
class ArticlesEndpointImpl @Inject private constructor(
    private val connectivity: ConnectivityWatcher
) : ArticlesEndpoint {
  private val random = SecureRandom()

  override fun articles(token: String?, count: Int): Observable<PagedCollection<Article>> {
    return createUnreliableObservable {
      Observable.fromCallable {
        val hasMore = random.nextBoolean()
        val total = if (hasMore) count else random.nextInt(count) + 1

        PagedCollection(
            items = List(total) { createRandomArticle(UUID.randomUUID().toString()) },
            token = given(hasMore) { UUID.randomUUID().toString() }
        )
      }
    }
  }

  override fun article(id: String): Observable<ArticleDetails> {
    return createUnreliableObservable {
      Observable.fromCallable {
        createRandomArticleDetails(id)
      }
    }
  }

  private fun createRandomArticle(id: String): Article {
    return Article(
        id = id,
        title = "Title $id",
        subtitle = "Subtitle $id"
    )
  }

  private fun createRandomArticleDetails(id: String): ArticleDetails {
    return ArticleDetails(
        id = id,
        title = "Title $id",
        subtitle = "Subtitle $id",
        content = "Content $id"
    )
  }

  private inline fun <T : Any> createUnreliableObservable(crossinline factory: () -> Observable<T>): Observable<T> {
    return Observable.defer {
      factory().delay(random.nextInt(5000).toLong(), TimeUnit.MILLISECONDS)
          .timeout(2500 + random.nextInt(2500).toLong(), TimeUnit.MILLISECONDS)
          .flatMap {
            if (connectivity.status() == ConnectivityStatus.OFFLINE) {
              Observable.error(RuntimeException("Offline ¯\\_(ツ)_/¯"))
            } else {
              Observable.just(it)
            }
          }
          .observeOn(mainThreadScheduler())

    }
  }
}
