package com.github.vmironov.news.bindings

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout
import com.github.vmironov.news.R

@BindingAdapter("colored")
fun setColored(view: SwipeRefreshLayout, colored: Boolean) {
  if (colored) {
    view.setColorSchemeColors(*view.resources.getIntArray(R.array.progress_values))
  }
}

@BindingAdapter("onRefresh")
fun setOnRefresh(view: SwipeRefreshLayout, command: ObservableCommand<Unit>?) {
  view.setOnRefreshListener { command?.execute(Unit) }
}
