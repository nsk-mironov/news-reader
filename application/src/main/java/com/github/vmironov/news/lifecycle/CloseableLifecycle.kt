package com.github.vmironov.news.lifecycle

enum class CloseableLifecycle : Lifecycle<CloseableLifecycle> {
  UNKNOWN, CREATE, DESTROY;

  override val valid: Boolean
    get() = this !== UNKNOWN

  override val expanding: Boolean
    get() = before(DESTROY)

  override fun next(): CloseableLifecycle {
    require(ordinal < events.lastIndex) { "No next event for $this" }
    return events[ordinal + 1]
  }

  override fun previous(): CloseableLifecycle {
    require(ordinal > 0) { "No previous event for $this" }
    return events[ordinal - 1]
  }

  override fun interval(): LifecycleInterval<CloseableLifecycle> {
    return when (this) {
      CREATE, DESTROY -> Interval.CREATED
      UNKNOWN -> throw IllegalArgumentException("No interval for $this")
    }
  }

  enum class Interval(
      override val start: CloseableLifecycle,
      override val endInclusive: CloseableLifecycle
  ) : LifecycleInterval<CloseableLifecycle> {
    CREATED(CREATE, DESTROY);
  }

  companion object {
    private val events = values()
  }
}
