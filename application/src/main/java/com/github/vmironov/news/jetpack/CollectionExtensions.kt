package com.github.vmironov.news.jetpack

fun <T> MutableCollection<T>.replaceAll(other: Collection<T>) {
  clear()
  addAll(other)
}
