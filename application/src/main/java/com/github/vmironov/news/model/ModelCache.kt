package com.github.vmironov.news.model

import android.support.v4.util.LruCache
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.references.SharedReference
import java.util.HashMap

interface ModelCache<K : Any, M : Model<*>> : CloseableLifecycleAware {
  fun acquire(key: K): M

  companion object {
    fun <K : Any, M : Model<*>> lru(clazz: Class<M>, size: Int = 8, factory: (K) -> M): ModelCache<K, M> {
      return LruModelCache(clazz, size, factory)
    }

    fun <K : Any, M : Model<*>> hard(clazz: Class<M>, factory: (K) -> M): ModelCache<K, M> {
      return LruModelCache(clazz, Int.MAX_VALUE, factory)
    }

    fun <K : Any, M : Model<*>> strict(clazz: Class<M>, factory: (K) -> M): ModelCache<K, M> {
      return StrictModelCache(clazz, factory)
    }
  }
}

private class StrictModelCache<K : Any, M : Model<*>>(
    private val clazz: Class<M>,
    private val factory: (K) -> M
) : ModelCache<K, M>, CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private val cache = HashMap<K, SharedReference<M>>(16)

  override fun acquire(key: K): M {
    return getOrCreateReference(key).acquire()
  }

  private fun getOrCreateReference(id: K): SharedReference<M> {
    return cache.getOrPut(id) {
      Models.share(clazz) { factory(id) }
    }
  }
}

private class LruModelCache<K : Any, M : Model<*>>(
    private val clazz: Class<M>,
    private val size: Int,
    private val factory: (K) -> M
) : ModelCache<K, M>, CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  private val cache = object : LruCache<K, Entry<M>>(size) {
    override fun create(key: K): Entry<M> {
      return Models.share(clazz) { factory(key) }.let {
        Entry(it, it.acquire())
      }
    }

    override fun entryRemoved(evicted: Boolean, key: K, old: Entry<M>?, new: Entry<M>?) {
      old?.value?.close()
    }
  }

  override fun acquire(key: K): M {
    return cache.get(key).reference.acquire()
  }

  private class Entry<M : Model<*>>(
      val reference: SharedReference<M>,
      val value: M
  )
}
