package com.github.vmironov.news.lifecycle

enum class ViewHolderLifecycle : Lifecycle<ViewHolderLifecycle> {
  UNKNOWN, ATTACH, DETACH;

  override val valid: Boolean
    get() = this !== UNKNOWN

  override val expanding: Boolean
    get() = before(DETACH)

  override fun next(): ViewHolderLifecycle {
    require(ordinal < events.lastIndex) { "No next event for $this" }
    return events[ordinal + 1]
  }

  override fun previous(): ViewHolderLifecycle {
    require(ordinal > 0) { "No previous event for $this" }
    return events[ordinal - 1]
  }

  override fun interval(): LifecycleInterval<ViewHolderLifecycle> {
    return when (this) {
      ATTACH, DETACH -> Interval.ATTACHED
      UNKNOWN -> throw IllegalArgumentException("No interval for $this")
    }
  }

  enum class Interval(
      override val start: ViewHolderLifecycle,
      override val endInclusive: ViewHolderLifecycle
  ) : LifecycleInterval<ViewHolderLifecycle> {
    ATTACHED(ATTACH, DETACH);
  }

  companion object {
    private val events = values()
  }
}
