package com.github.vmironov.news.endpoints

import io.michaelrocks.lightsaber.Module
import io.michaelrocks.lightsaber.Provides

@Module
class EndpointsModule {
  @Provides fun providesArticlesEndpoint(impl: ArticlesEndpointImpl): ArticlesEndpoint {
    return impl
  }
}
