package com.github.vmironov.news.ui.fetching

import android.support.v4.app.Fragment
import com.github.vmironov.news.jetpack.findOrAddFragment
import com.github.vmironov.news.model.Model

fun Fragment.addFetchingFragment(container: Int, model: Model<*>): FetchingFragment {
  return findOrAddFragment(container) { FetchingFragment.newInstance() }.also {
    it.model = model
  }
}
