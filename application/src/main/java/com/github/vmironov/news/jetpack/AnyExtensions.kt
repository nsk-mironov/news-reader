package com.github.vmironov.news.jetpack

inline fun <reified T : Any> Any?.cast(): T {
  return this as T
}

inline fun <reified T : Any> Any?.castOptional(): T? {
  return this as? T
}

inline fun <T : Any> given(condition: Boolean, body: () -> T?): T? {
  return if (condition) body() else null
}

fun <T> uninitialized(): T {
  return null as T
}

fun Any.asTrue() = true
fun Any.asFalse() = false
fun Any.asUnit() = Unit
