package com.github.vmironov.news.lifecycle

enum class ActivityLifecycle : Lifecycle<ActivityLifecycle> {
  UNKNOWN, CREATE, START, RESUME, PAUSE, STOP, DESTROY;

  override val valid: Boolean
    get() = this !== UNKNOWN

  override val expanding: Boolean
    get() = before(PAUSE)

  override fun next(): ActivityLifecycle {
    require(ordinal < events.lastIndex) { "No next event for $this" }
    return events[ordinal + 1]
  }

  override fun previous(): ActivityLifecycle {
    require(ordinal > 0) { "No previous event for $this" }
    return events[ordinal - 1]
  }

  override fun interval(): LifecycleInterval<ActivityLifecycle> {
    return when (this) {
      CREATE, DESTROY -> Interval.CREATED
      START, STOP -> Interval.STARTED
      RESUME, PAUSE -> Interval.RESUMED
      UNKNOWN -> throw IllegalArgumentException("No interval for $this")
    }
  }

  enum class Interval(
      override val start: ActivityLifecycle,
      override val endInclusive: ActivityLifecycle
  ) : LifecycleInterval<ActivityLifecycle> {
    CREATED(CREATE, DESTROY),
    STARTED(START, STOP),
    RESUMED(RESUME, PAUSE)
  }

  companion object {
    private val events = values()
  }
}
