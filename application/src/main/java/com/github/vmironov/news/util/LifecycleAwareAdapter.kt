package com.github.vmironov.news.util

import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.vmironov.news.bindings.ObservableModel
import com.github.vmironov.news.bindings.ObservableModelMixin
import com.github.vmironov.news.lifecycle.CloseableLifecycleAware
import com.github.vmironov.news.lifecycle.CloseableLifecycleAwareMixin
import com.github.vmironov.news.lifecycle.DelegateLifecycleAware
import com.github.vmironov.news.lifecycle.LifecycleAwareDelegate
import com.github.vmironov.news.lifecycle.ViewHolderLifecycle

abstract class LifecycleAwareAdapter<VH : LifecycleAwareViewHolder> :
    RecyclerView.Adapter<VH>(),
    CloseableLifecycleAware by CloseableLifecycleAwareMixin() {
  override fun onViewAttachedToWindow(holder: VH) {
    holder.attach()
  }

  override fun onViewDetachedFromWindow(holder: VH) {
    holder.detach()
  }

  override fun onViewRecycled(holder: VH) {
    holder.detach()
  }
}

abstract class LifecycleAwareViewHolder(
    view: View
) : RecyclerView.ViewHolder(view),
    DelegateLifecycleAware<ViewHolderLifecycle>,
    ObservableModel by ObservableModelMixin() {

  override val lifecycle = LifecycleAwareDelegate(ViewHolderLifecycle.UNKNOWN)

  fun attach() {
    lifecycle.notifyLifecycleEvent(ViewHolderLifecycle.ATTACH)
  }

  fun detach() {
    lifecycle.notifyLifecycleEvent(ViewHolderLifecycle.DETACH)
  }
}
