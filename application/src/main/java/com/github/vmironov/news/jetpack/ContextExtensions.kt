package com.github.vmironov.news.jetpack

import android.content.Context
import android.widget.Toast

fun Context.toast(message: CharSequence): Toast {
  return Toast.makeText(this, message, Toast.LENGTH_SHORT).apply(Toast::show)
}
