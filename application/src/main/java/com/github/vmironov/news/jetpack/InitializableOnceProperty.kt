package com.github.vmironov.news.jetpack

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

@Suppress("NOTHING_TO_INLINE")
inline fun <T> initializableOnce(): ReadWriteProperty<Any?, T> {
  return InitializableOnceProperty()
}

inline fun <T> initializableOnce(crossinline observer: (T) -> Unit): ReadWriteProperty<Any?, T> {
  return object : InitializableOnceProperty<T>() {
    override fun onInitialized(value: T) = observer(value)
  }
}

open class InitializableOnceProperty<T> : ReadWriteProperty<Any?, T> {
  private var value: Any? = EmptyObject

  override fun getValue(thisRef: Any?, property: KProperty<*>): T {
    check(value != EmptyObject) { "Property ${property.name} not initialized" }
    @Suppress("UNCHECKED_CAST")
    return value as T
  }

  override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
    check(this.value === EmptyObject) { "Property ${property.name} already initialized" }
    this.value = value
    onInitialized(value)
  }

  protected open fun onInitialized(value: T) = Unit
}

private object EmptyObject