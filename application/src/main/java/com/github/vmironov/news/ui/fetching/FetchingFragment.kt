package com.github.vmironov.news.ui.fetching

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.vmironov.news.bindings.FetchingObservableCommand
import com.github.vmironov.news.bindings.property
import com.github.vmironov.news.core.Optional
import com.github.vmironov.news.databinding.FetchingFragmentBinding
import com.github.vmironov.news.jetpack.cast
import com.github.vmironov.news.jetpack.initializableOnce
import com.github.vmironov.news.jetpack.toast
import com.github.vmironov.news.jetpack.uninitialized
import com.github.vmironov.news.lifecycle.FragmentLifecycle
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindToLifecycle
import com.github.vmironov.news.model.Model
import com.github.vmironov.news.rx.observe
import com.github.vmironov.news.ui.base.BindingFragment
import com.github.vmironov.news.util.ErrorDescriptor
import javax.inject.Inject

class FetchingFragment : BindingFragment<FetchingFragmentBinding>(), FetchingViewModel {
  @Inject private val errors: ErrorDescriptor = uninitialized()

  private var hasData by property(false, all = true)
  private var hasError by property(false, all = true)
  private var refreshing by property(false, all = true)

  override var errorTitle by property<CharSequence>("")
  override var errorMessage by property<CharSequence>("")

  override val showContainer: Boolean
    get() = showRefreshing || showError

  override val showRefreshing: Boolean
    get() = !hasData && refreshing

  override val showError: Boolean
    get() = !hasData && !refreshing && hasError

  override val onRefreshClick by bindToLifecycle(FragmentLifecycle.Interval.CREATED) {
    FetchingObservableCommand(model)
  }

  var model by initializableOnce<Model<*>> { model ->
    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.values.observe {
        hasData = it.cast<Optional<Any>>().unwrap() != null
        hasError = !hasData && hasError
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.fetching.observe {
        refreshing = it
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      model.errors.observe {
        hasError = !hasData

        if (hasError) {
          errorTitle = errors.getHumanReadableErrorTitle(it)
          errorMessage = errors.getHumanReadableErrorMessage(it)
        }

        if (!hasError) {
          context.toast(errors.getHumanReadableErrorToast(it))
        }
      }
    }
  }

  override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): FetchingFragmentBinding {
    return FetchingFragmentBinding.inflate(inflater, container, false)
  }

  override fun onBindingCreated(binding: FetchingFragmentBinding, savedInstanceState: Bundle?) {
    binding.model = this
  }

  companion object {
    fun newInstance(): FetchingFragment {
      return FetchingFragment()
    }
  }
}
