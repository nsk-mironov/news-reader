package com.github.vmironov.news.jetpack

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

inline fun <reified T : Fragment> FragmentActivity.findOrAddFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return supportFragmentManager.findOrAddFragment(container, tag, creator)
}

inline fun <reified T : Fragment> FragmentActivity.findFragment(tag: String = T::class.java.name): T? {
  return supportFragmentManager.findFragment(tag)
}

inline fun <reified T : Fragment> FragmentActivity.addFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return supportFragmentManager.addFragment(container, tag, creator)
}

inline fun <reified T : Fragment> Fragment.findOrAddFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return childFragmentManager.findOrAddFragment(container, tag, creator)
}

inline fun <reified T : Fragment> Fragment.findFragment(tag: String = T::class.java.name): T? {
  return childFragmentManager.findFragment(tag)
}

inline fun <reified T : Fragment> Fragment.addFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return childFragmentManager.addFragment(container, tag, creator)
}

inline fun <reified T : Fragment> FragmentManager.findOrAddFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return findFragment<T>(tag) ?: addFragment(container, tag, creator)
}

inline fun <reified T : Fragment> FragmentManager.findFragment(tag: String = T::class.java.name): T? {
  return findFragmentByTag(tag).castOptional()
}

inline fun <reified T : Fragment> FragmentManager.addFragment(container: Int, tag: String = T::class.java.name, creator: () -> T): T {
  return creator().also {
    beginTransaction().replace(container, it, tag).commit()
  }
}
