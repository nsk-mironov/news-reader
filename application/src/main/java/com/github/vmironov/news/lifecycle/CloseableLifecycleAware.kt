package com.github.vmironov.news.lifecycle

import com.github.vmironov.news.core.Event
import com.github.vmironov.news.core.event
import java.io.Closeable

interface CloseableLifecycleAware : LifecycleAware<CloseableLifecycle>, Closeable

class CloseableLifecycleAwareMixin : CloseableLifecycleAware {
  override var lifecycleState = CloseableLifecycle.CREATE
  override val onLifecycleStateChanged: Event<CloseableLifecycle> = event { lifecycleState }

  override fun close() {
    if (lifecycleState != CloseableLifecycle.DESTROY) {
      lifecycleState = CloseableLifecycle.DESTROY
      onLifecycleStateChanged(CloseableLifecycle.DESTROY, true)
    }
  }
}
