package com.github.vmironov.news.bindings

import android.databinding.Observable
import android.databinding.PropertyChangeRegistry

fun ObservableModel.notifyPropertyChanged(fieldId: Int) = notifyPropertyChanged(this, fieldId)
fun ObservableModel.notifyChange() = notifyChange(this)

interface ObservableModel : Observable {
  fun notifyPropertyChanged(container: Observable, fieldId: Int)
  fun notifyChange(container: Observable)
}

class ObservableModelMixin : PropertyChangeRegistry(), ObservableModel {
  override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    add(callback)
  }

  override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
    remove(callback)
  }

  override fun notifyChange(container: Observable) {
    notifyCallbacks(container, 0, null)
  }

  override fun notifyPropertyChanged(container: Observable, fieldId: Int) {
    notifyCallbacks(container, fieldId, null)
  }
}
