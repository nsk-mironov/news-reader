package com.github.vmironov.news.lifecycle

import com.github.vmironov.news.core.Event
import com.github.vmironov.news.core.event
import com.github.vmironov.news.rx.observe
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.io.Closeable
import kotlin.properties.ReadOnlyProperty

interface LifecycleAware<L : Lifecycle<L>> {
  val lifecycleState: L
  val onLifecycleStateChanged: Event<L>
}

class LifecycleAwareDelegate<L : Lifecycle<L>>(default: L) : LifecycleAware<L> {
  override var lifecycleState = default
    private set

  override val onLifecycleStateChanged: Event<L> = event {
    lifecycleState
  }

  fun notifyLifecycleEvent(event: L) {
    if (lifecycleState != event) {
      lifecycleState = event
      onLifecycleStateChanged(event, event.collapsing)
    }
  }
}

interface DelegateLifecycleAware<L : Lifecycle<L>> : LifecycleAware<L> {
  val lifecycle: LifecycleAware<L>

  override val lifecycleState: L
    get() = lifecycle.lifecycleState

  override val onLifecycleStateChanged: Event<L>
    get() = lifecycle.onLifecycleStateChanged
}

inline fun <T : Closeable, L : Lifecycle<L>> LifecycleAware<L>.bindToLifecycle(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> T
): ReadOnlyProperty<Any, T> {
  return object : LifecycleAwareProperty<T, L>(this, interval, false) {
    override fun onAcquire(): T = acquire()
    override fun onRelease(value: T) = value.close()
  }
}

inline fun <T : Closeable, L : Lifecycle<L>> LifecycleAware<L>.bindToLifecycleEagerly(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> T
): ReadOnlyProperty<Any, T> {
  return object : LifecycleAwareProperty<T, L>(this, interval, true) {
    override fun onAcquire(): T = acquire()
    override fun onRelease(value: T) = value.close()
  }
}

inline fun <L : Lifecycle<L>> LifecycleAware<L>.bindObservableToLifecycleEagerly(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> Observable<*>
): ReadOnlyProperty<Any, Disposable> {
  return object : LifecycleAwareProperty<Disposable, L>(this, interval, true) {
    override fun onAcquire(): Disposable = acquire().observe()
    override fun onRelease(value: Disposable) = value.dispose()
  }
}

inline fun <T : Disposable, L : Lifecycle<L>> LifecycleAware<L>.bindDisposableToLifecycleEagerly(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> T
): ReadOnlyProperty<Any, T> {
  return object : LifecycleAwareProperty<T, L>(this, interval, true) {
    override fun onAcquire(): T = acquire()
    override fun onRelease(value: T) = value.dispose()
  }
}

inline fun <T, L : Lifecycle<L>> LifecycleAware<L>.bindToLifecycle(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> T,
    crossinline release: (T) -> Unit
): ReadOnlyProperty<Any, T> {
  return object : LifecycleAwareProperty<T, L>(this, interval, false) {
    override fun onAcquire(): T = acquire()
    override fun onRelease(value: T) = release(value)
  }
}

inline fun <T, L : Lifecycle<L>> LifecycleAware<L>.bindToLifecycleEagerly(
    interval: LifecycleInterval<L>,
    crossinline acquire: () -> T,
    crossinline release: (T) -> Unit
): ReadOnlyProperty<Any, T> {
  return object : LifecycleAwareProperty<T, L>(this, interval, true) {
    override fun onAcquire(): T = acquire()
    override fun onRelease(value: T) = release(value)
  }
}
