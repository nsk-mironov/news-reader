package com.github.vmironov.news.storage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.github.vmironov.news.core.Article
import com.github.vmironov.news.core.ArticleDetails
import com.github.vmironov.news.jetpack.asUnit
import com.github.vmironov.news.jetpack.given
import com.github.vmironov.news.rx.mainThreadScheduler
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

interface ArticleStorage {
  fun articles(): Observable<List<Article>>

  fun article(id: String): Observable<ArticleDetails>

  fun replace(articles: List<Article>): Observable<Unit>

  fun replace(article: ArticleDetails): Observable<Unit>
}

@Singleton
class ArticleStorageImpl @Inject private constructor(
    private val context: Context
) : ArticleStorage {
  private val invalidations = PublishSubject.create<Unit>()

  private val helper by lazy {
    createSQLiteOpenHelper()
  }

  override fun articles(): Observable<List<Article>> {
    return invalidations.startWith(Unit).switchMap {
      createQueryArticlesObservable()
          .subscribeOn(Schedulers.io())
          .observeOn(mainThreadScheduler())
    }
  }

  override fun article(id: String): Observable<ArticleDetails> {
    return invalidations.startWith(Unit).switchMap {
      createQueryArticleDetailsObservable(id)
          .subscribeOn(Schedulers.io())
          .observeOn(mainThreadScheduler())
    }
  }

  override fun replace(articles: List<Article>): Observable<Unit> {
    return createReplaceArticlesObservable(articles)
        .subscribeOn(Schedulers.io())
        .observeOn(mainThreadScheduler())
        .doOnNext { invalidations.onNext(Unit) }
  }

  override fun replace(article: ArticleDetails): Observable<Unit> {
    return createReplaceArticleDetailsObservable(article)
        .subscribeOn(Schedulers.io())
        .observeOn(mainThreadScheduler())
        .doOnNext { invalidations.onNext(Unit) }
  }

  private fun createQueryArticlesObservable(): Observable<List<Article>> {
    return Observable.fromCallable {
      val result = ArrayList<Article>()

      helper.readableDatabase.query("articles", null, null, null, null, null, null).use {
        while (it.moveToNext()) {
          result.add(Article(
              id = it.getString(it.getColumnIndexOrThrow("id")),
              title = it.getString(it.getColumnIndexOrThrow("title")),
              subtitle = it.getString(it.getColumnIndexOrThrow("subtitle")),
              image = it.getString(it.getColumnIndexOrThrow("image"))
          ))
        }
      }

      result
    }
  }

  private fun createQueryArticleDetailsObservable(article: String): Observable<ArticleDetails> {
    return Observable.defer {
      helper.readableDatabase.query("details", null, "id = ?", arrayOf(article), null, null, null).use {
        Observable.fromIterable(listOfNotNull(given(it.moveToFirst()) {
          ArticleDetails(
              id = it.getString(it.getColumnIndexOrThrow("id")),
              title = it.getString(it.getColumnIndexOrThrow("title")),
              subtitle = it.getString(it.getColumnIndexOrThrow("subtitle")),
              image = it.getString(it.getColumnIndexOrThrow("image")),
              content = it.getString(it.getColumnIndexOrThrow("content"))
          )
        }))
      }
    }
  }

  private fun createReplaceArticlesObservable(articles: List<Article>): Observable<Unit> {
    return Observable.fromCallable {
      helper.writableDatabase.transactional {
        delete("articles", null, null)

        for (article in articles) {
          insert("articles", null, createContentValues(article))
        }
      }
    }
  }

  private fun createReplaceArticleDetailsObservable(article: ArticleDetails): Observable<Unit> {
    return Observable.fromCallable {
      val values = createContentValues(article)
      val database = helper.writableDatabase
      val conflict = SQLiteDatabase.CONFLICT_REPLACE

      database.insertWithOnConflict("details", null, values, conflict).asUnit()
    }
  }

  private fun createSQLiteOpenHelper(): SQLiteOpenHelper {
    return object : SQLiteOpenHelper(context, "articles", null, 1) {
      override fun onCreate(database: SQLiteDatabase) {
        database.execSQL("create table articles (id text primary key, title text, subtitle text, image text)")
        database.execSQL("create table details (id text primary key, title text, subtitle text, image text, content text)")
      }

      override fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // nothing to do at the moment
      }
    }
  }

  private fun createContentValues(article: Article): ContentValues {
    return ContentValues().apply {
      put("id", article.id)
      put("title", article.title)
      put("subtitle", article.subtitle)
      put("image", article.image)
    }
  }

  private fun createContentValues(article: ArticleDetails): ContentValues {
    return ContentValues().apply {
      put("id", article.id)
      put("title", article.title)
      put("subtitle", article.subtitle)
      put("image", article.image)
      put("content", article.content)
    }
  }

  private inline fun SQLiteDatabase.transactional(action: SQLiteDatabase.() -> Unit) {
    beginTransaction()

    try {
      apply(action).setTransactionSuccessful()
    } finally {
      endTransaction()
    }
  }
}
