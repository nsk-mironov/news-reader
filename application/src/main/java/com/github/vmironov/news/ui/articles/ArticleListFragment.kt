package com.github.vmironov.news.ui.articles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.github.vmironov.news.R
import com.github.vmironov.news.bindings.FetchingObservableCommand
import com.github.vmironov.news.bindings.property
import com.github.vmironov.news.databinding.ArticleListFragmentBinding
import com.github.vmironov.news.jetpack.ignoreErrors
import com.github.vmironov.news.jetpack.uninitialized
import com.github.vmironov.news.lifecycle.FragmentLifecycle
import com.github.vmironov.news.lifecycle.bindDisposableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindObservableToLifecycleEagerly
import com.github.vmironov.news.lifecycle.bindToLifecycleEagerly
import com.github.vmironov.news.model.RootModel
import com.github.vmironov.news.rx.observe
import com.github.vmironov.news.ui.ArticleDetailsActivity
import com.github.vmironov.news.ui.base.BindingFragment
import com.github.vmironov.news.ui.expanding.addExpandingFragment
import com.github.vmironov.news.ui.fetching.addFetchingFragment
import com.github.vmironov.news.util.Layouts
import javax.inject.Inject

class ArticleListFragment : BindingFragment<ArticleListFragmentBinding>(), ArticleListViewModel {
  @Inject private val root: RootModel = uninitialized()
  @Inject private val factory: ArticleAdapterFactory = uninitialized()

  private val collection by bindToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
    root.acquireArticleListModel()
  }

  private val adapter by bindToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
    factory.create(context, collection)
  }

  override var loading by property(true)
  override var empty by property(false)

  override val onRefreshClick by bindToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
    FetchingObservableCommand(collection)
  }

  init {
    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.VIEW_CREATED) {
      adapter.clicks().observe {
        context.startActivity(ArticleDetailsActivity.newIntent(context, it.id))
      }
    }

    bindDisposableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      collection.values.observe {
        empty = it.unwrap()?.isEmpty() ?: false
        loading = it.unwrap() == null
      }
    }

    bindObservableToLifecycleEagerly(FragmentLifecycle.Interval.CREATED) {
      collection.fetch().ignoreErrors()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    addExpandingFragment(R.id.expanding, collection, R.id.recycler)
    addFetchingFragment(R.id.container, collection)
  }

  override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): ArticleListFragmentBinding {
    return ArticleListFragmentBinding.inflate(inflater, container, false)
  }

  override fun onBindingCreated(binding: ArticleListFragmentBinding, savedInstanceState: Bundle?) {
    super.onBindingCreated(binding, savedInstanceState)

    binding.recycler.layoutManager = Layouts.verticalList(context)
    binding.recycler.adapter = adapter

    binding.model = this
  }

  companion object {
    fun newInstance(): ArticleListFragment {
      return ArticleListFragment()
    }
  }
}
