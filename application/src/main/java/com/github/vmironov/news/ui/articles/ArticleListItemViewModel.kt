package com.github.vmironov.news.ui.articles

import android.databinding.Bindable
import com.github.vmironov.news.bindings.ObservableModel

interface ArticleListItemViewModel : ObservableModel {
  @get:Bindable val title: CharSequence
  @get:Bindable val subtitle: CharSequence
  @get:Bindable val image: String
}
