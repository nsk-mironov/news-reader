package com.github.vmironov.news.jetpack

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment

const val EXTRA_ARGUMENT = "com.joom.intent.extra.EXTRA_ARGUMENT"

inline fun <reified F : Fragment> fragment(arguments: Parcelable): F {
  return F::class.java.newInstance().also {
    it.arguments = Bundle(1).apply {
      putParcelable(EXTRA_ARGUMENT, arguments)
    }
  }
}

inline fun <reified A : Activity> intent(context: Context, arguments: Parcelable): Intent {
  return Intent(context, A::class.java).also {
    it.putExtra(EXTRA_ARGUMENT, arguments)
  }
}

inline fun <reified A : Parcelable> Fragment.getTypedArguments(): A {
  return arguments.getParcelable<A>(EXTRA_ARGUMENT)
}

inline fun <reified A : Parcelable> Activity.getTypedArguments(): A {
  return intent.getParcelableExtra<A>(EXTRA_ARGUMENT)
}
