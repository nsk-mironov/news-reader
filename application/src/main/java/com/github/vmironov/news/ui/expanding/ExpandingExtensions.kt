package com.github.vmironov.news.ui.expanding

import android.support.v4.app.Fragment
import com.github.vmironov.news.jetpack.findOrAddFragment
import com.github.vmironov.news.model.CollectionModel

fun Fragment.addExpandingFragment(container: Int, model: CollectionModel<*>, recycler: Int): ExpandingFragment {
  return findOrAddFragment(container) { ExpandingFragment.newInstance(recycler) }.also {
    it.model = model
  }
}
