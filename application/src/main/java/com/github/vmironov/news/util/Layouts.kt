package com.github.vmironov.news.util

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager

object Layouts {
  fun verticalGrid(context: Context, columns: Int, recycleOnDetach: Boolean = true): GridLayoutManager {
    return GridLayoutManager(context, columns, GridLayoutManager.VERTICAL, false).apply {
      recycleChildrenOnDetach = recycleOnDetach
    }
  }

  fun horizontalGrid(context: Context, columns: Int, recycleOnDetach: Boolean = true): GridLayoutManager {
    return GridLayoutManager(context, columns, GridLayoutManager.HORIZONTAL, false).apply {
      recycleChildrenOnDetach = recycleOnDetach
    }
  }

  fun verticalList(context: Context, recycleOnDetach: Boolean = true): LinearLayoutManager {
    return LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false).apply {
      recycleChildrenOnDetach = recycleOnDetach
    }
  }

  fun horizontalList(context: Context, recycleOnDetach: Boolean = true): LinearLayoutManager {
    return LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false).apply {
      recycleChildrenOnDetach = recycleOnDetach
    }
  }
}
