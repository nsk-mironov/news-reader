package com.github.vmironov.news

import android.app.Application
import com.github.vmironov.news.jetpack.uninitialized
import com.github.vmironov.news.model.RootModel
import com.github.vmironov.news.util.InjectorHolder
import javax.inject.Inject

class RedditApplication : Application() {
  @Inject private val root: RootModel = uninitialized()

  override fun onCreate() {
    super.onCreate()

    val component = ApplicationComponent(this)
    val injector = InjectorHolder.getOrCreateInjector(component)

    injector.injectMembers(this)
  }
}
