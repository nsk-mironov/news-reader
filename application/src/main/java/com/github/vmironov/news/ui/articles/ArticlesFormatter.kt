package com.github.vmironov.news.ui.articles

import android.content.Context
import com.github.vmironov.news.core.Article
import com.github.vmironov.news.core.ArticleDetails
import javax.inject.Inject

class ArticlesFormatter @Inject private constructor(
    private val context: Context
) {
  fun formatTitle(article: Article): CharSequence {
    return article.title
  }

  fun formatSubtitle(article: Article): CharSequence {
    return article.subtitle
  }

  fun formatImageUrl(article: Article): String {
    return article.image
  }

  fun formatTitle(article: ArticleDetails): CharSequence {
    return article.title
  }

  fun formatSubtitle(article: ArticleDetails): CharSequence {
    return article.subtitle
  }

  fun formatImageUrl(article: ArticleDetails): String {
    return article.image
  }

  fun formatContent(article: ArticleDetails): CharSequence {
    return article.content
  }
}
