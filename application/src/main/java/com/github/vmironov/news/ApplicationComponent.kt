package com.github.vmironov.news

import android.app.Application
import com.github.vmironov.news.endpoints.EndpointsModule
import com.github.vmironov.news.model.ModelModule
import io.michaelrocks.lightsaber.Component
import io.michaelrocks.lightsaber.Provides

@Component
class ApplicationComponent(private val application: Application) {
  @Provides fun provideApplicationModule(): ApplicationModule {
    return ApplicationModule(application)
  }

  @Provides fun provideEndpointsModule(): EndpointsModule {
    return EndpointsModule()
  }

  @Provides fun provideModelModule(): ModelModule {
    return ModelModule()
  }
}
