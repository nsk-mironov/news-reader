package com.github.vmironov.news.model

import com.github.vmironov.news.references.SharedReference
import javax.inject.Provider

object Models {
  fun <T : Model<*>> share(clazz: Class<T>, provider: Provider<out T>): SharedReference<T> {
    return share(clazz) { provider.get() }
  }

  fun <T : Model<*>> share(clazz: Class<T>, factory: () -> T): SharedReference<T> {
    return SharedReference.createUnsafe(clazz, factory)
  }
}
