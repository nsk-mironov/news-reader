package com.github.vmironov.news.rx

import io.reactivex.CompletableObserver
import io.reactivex.MaybeObserver
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.atomic.AtomicReference

@Suppress("NOTHING_TO_INLINE")
inline fun <T : Any> Observable<T>.observe(): Disposable {
  return subscribeWith(SimpleObserver<T>())
}

inline fun <T : Any> Observable<T>.observe(
    crossinline onNext: (T) -> Unit
): Disposable {
  return subscribeWith(object : SimpleObserver<T>() {
    override fun onNext(value: T) = onNext(value)
  })
}

inline fun <T : Any> Observable<T>.observe(
    crossinline onNext: (T) -> Unit,
    crossinline onError: (Throwable) -> Unit
): Disposable {
  return subscribeWith(object : SimpleObserver<T>() {
    override fun onNext(value: T) = onNext(value)
    override fun onError(error: Throwable) = onError(error)
  })
}

inline fun <T : Any> Observable<T>.observe(
    crossinline onNext: (T) -> Unit,
    crossinline onError: (Throwable) -> Unit,
    crossinline onComplete: () -> Unit
): Disposable {
  return subscribeWith(object : SimpleObserver<T>() {
    override fun onNext(value: T) = onNext(value)
    override fun onError(error: Throwable) = onError(error)
    override fun onComplete() = onComplete()
  })
}

open class SimpleObserver<T> : AtomicReference<Disposable>(), Observer<T>, SingleObserver<T>, MaybeObserver<T>, CompletableObserver, Disposable {
  override fun onNext(value: T) = Unit
  override fun onSuccess(value: T) = Unit
  override fun onError(error: Throwable) = onUnhandledError.onNext(error)
  override fun onComplete() = Unit

  override final fun onSubscribe(disposable: Disposable) {
    DisposableHelper.set(this, disposable)
  }

  override final fun dispose() {
    DisposableHelper.dispose(this)
  }

  override final fun isDisposed(): Boolean {
    return DisposableHelper.isDisposed(get())
  }

  companion object : SimpleObserver<Unit>() {
    val onUnhandledError = PublishSubject.create<Throwable>()
  }
}
