package com.github.vmironov.news.lifecycle

enum class FragmentLifecycle : Lifecycle<FragmentLifecycle> {
  UNKNOWN, ATTACH, CREATE, VIEW_CREATE, START, RESUME, PAUSE, STOP, VIEW_DESTROY, DESTROY, DETACH;

  override val valid: Boolean
    get() = this !== UNKNOWN

  override val expanding: Boolean
    get() = before(RESUME)

  override fun next(): FragmentLifecycle {
    require(ordinal < events.lastIndex) { "No next event for $this" }
    return events[ordinal + 1]
  }

  override fun previous(): FragmentLifecycle {
    require(ordinal > 0) { "No previous event for $this" }
    return events[ordinal - 1]
  }

  override fun interval(): LifecycleInterval<FragmentLifecycle> {
    return when (this) {
      ATTACH, DETACH -> Interval.ATTACHED
      CREATE, DESTROY -> Interval.CREATED
      VIEW_CREATE, VIEW_DESTROY -> Interval.VIEW_CREATED
      START, STOP -> Interval.STARTED
      RESUME, PAUSE -> Interval.RESUMED
      UNKNOWN -> throw IllegalArgumentException("No interval for $this")
    }
  }

  enum class Interval(
      override val start: FragmentLifecycle,
      override val endInclusive: FragmentLifecycle
  ) : LifecycleInterval<FragmentLifecycle> {
    ATTACHED(ATTACH, DETACH),
    CREATED(CREATE, DESTROY),
    VIEW_CREATED(VIEW_CREATE, VIEW_DESTROY),
    STARTED(START, STOP),
    RESUMED(RESUME, PAUSE)
  }

  companion object {
    private val events = values()
  }
}
