package com.github.vmironov.news.ui.articles

import android.databinding.Bindable
import com.github.vmironov.news.bindings.ObservableCommand
import com.github.vmironov.news.bindings.ObservableModel

interface ArticleListViewModel : ObservableModel {
  @get:Bindable val loading: Boolean
  @get:Bindable val empty: Boolean

  @get:Bindable val onRefreshClick: ObservableCommand<Unit>
}
