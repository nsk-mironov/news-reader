package com.github.vmironov.news.bindings

import android.databinding.DataBindingUtil
import java.util.HashMap

val EMPTY_ARRAY_INT = intArrayOf()
val EMPTY_ARRAY_STRING = arrayOf<String>()

object DataBindingMapping {
  private val MAPPING = HashMap<String, Int>(256)

  init {
    for (index in 0..Int.MAX_VALUE) {
      if (DataBindingUtil.convertBrIdToString(index) != null) {
        MAPPING[DataBindingUtil.convertBrIdToString(index)] = index
      } else {
        break
      }
    }
  }

  fun convertToBinding(name: String): Int {
    return MAPPING[name] ?: throw IllegalArgumentException("Unknown property name \"$name\")")
  }

  fun convertToBindings(names: Array<out String>): IntArray {
    return if (!names.isEmpty()) {
      IntArray(names.size) { convertToBinding(names[it]) }
    } else {
      EMPTY_ARRAY_INT
    }
  }
}
