package com.github.vmironov.news.util

import java.util.HashMap
import java.util.concurrent.atomic.AtomicLong

class StableIds<T, K>(private val selector: (T) -> K) {
  private val cache = HashMap<K, Long>(32)
  private val counter = AtomicLong()

  fun from(value: T): Long {
    return cache.getOrPut(selector(value)) { next() }
  }

  private fun next(): Long {
    return counter.andIncrement
  }
}
