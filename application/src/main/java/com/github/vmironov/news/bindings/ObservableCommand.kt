package com.github.vmironov.news.bindings

import android.databinding.Bindable

interface ObservableCommand<I> : ObservableModel {
  companion object {
    inline fun <I> create(crossinline action: (I) -> Unit): ObservableCommand<I> {
      return object : BaseObservableCommand<I>() {
        override fun execute(argument: I) = action(argument)
      }
    }
  }

  @get:Bindable val enabled: Boolean
  @get:Bindable val executing: Boolean

  fun execute(argument: I)
}

abstract class BaseObservableCommand<I> : ObservableCommand<I>, ObservableModel by ObservableModelMixin() {
  override var enabled by property(true)
  override var executing by property(false)
}
